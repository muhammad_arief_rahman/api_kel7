/*
SQLyog Ultimate v13.1.1 (64 bit)
MySQL - 10.4.22-MariaDB : Database - db_kelompok_7
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`db_kelompok_7` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;

USE `db_kelompok_7`;

/*Table structure for table `barang` */

DROP TABLE IF EXISTS `barang`;

CREATE TABLE `barang` (
  `kodeBarang` char(12) NOT NULL,
  `namaBarang` varchar(255) DEFAULT NULL,
  `jumlah` int(5) DEFAULT NULL,
  PRIMARY KEY (`kodeBarang`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

/*Data for the table `barang` */

insert  into `barang`(`kodeBarang`,`namaBarang`,`jumlah`) values 
('b001','meja',10),
('KM092','Camera',4),
('KM360','Kamera 360',3),
('MJ001','Meja Tamu',20),
('MJ120','Meja Bundar',10),
('tes','tes',1),
('VB123','Vas Bunga',9),
('VB2212','Vas Bunga',1);

/*Table structure for table `data_pengajuan_detail` */

DROP TABLE IF EXISTS `data_pengajuan_detail`;

CREATE TABLE `data_pengajuan_detail` (
  `kodePengajuan` char(12) NOT NULL,
  `kodeBarang` char(12) DEFAULT NULL,
  `jumlah` int(3) DEFAULT NULL,
  PRIMARY KEY (`kodePengajuan`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

/*Data for the table `data_pengajuan_detail` */

/*Table structure for table `data_pengembalian_detail` */

DROP TABLE IF EXISTS `data_pengembalian_detail`;

CREATE TABLE `data_pengembalian_detail` (
  `kodePengembalian` char(12) NOT NULL,
  `kodeBarang` char(12) DEFAULT NULL,
  `jumlah` int(11) DEFAULT NULL,
  PRIMARY KEY (`kodePengembalian`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

/*Data for the table `data_pengembalian_detail` */

/*Table structure for table `pengajuan` */

DROP TABLE IF EXISTS `pengajuan`;

CREATE TABLE `pengajuan` (
  `kodePengajuan` char(12) NOT NULL,
  `tanggal` date DEFAULT NULL,
  `npmPeminjam` char(12) DEFAULT NULL,
  `namaPeminjam` varchar(255) DEFAULT NULL,
  `prodi` varchar(255) DEFAULT NULL,
  `noHandphone` char(20) DEFAULT NULL,
  PRIMARY KEY (`kodePengajuan`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

/*Data for the table `pengajuan` */

insert  into `pengajuan`(`kodePengajuan`,`tanggal`,`npmPeminjam`,`namaPeminjam`,`prodi`,`noHandphone`) values 
('A1108','2023-06-11','21311083','Stephen Kohl','Sastra Komputer','0812345678'),
('M1103','2023-06-12','20111035','Melanie Putri','Sastra Komputer','85712342145'),
('MA1123','2023-06-17','20311432','Marsya Ajha','Sistem Informasi','088976543765'),
('S1109','2023-06-02','22311098','Samsul Hutapea','Sistem Informasi','0878654321');

/*Table structure for table `pengembalian` */

DROP TABLE IF EXISTS `pengembalian`;

CREATE TABLE `pengembalian` (
  `kodePengembalian` char(12) NOT NULL,
  `kodePengajuan` char(12) DEFAULT NULL,
  `tanggalKembali` date DEFAULT NULL,
  PRIMARY KEY (`kodePengembalian`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

/*Data for the table `pengembalian` */

insert  into `pengembalian`(`kodePengembalian`,`kodePengajuan`,`tanggalKembali`) values 
('','S110','2023-03-21'),
('A1108P','A1109','2023-06-19'),
('S1109P','S1109','2023-06-15'),
('T0000P','T0000','2023-06-16'),
('tes','tes3','2023-05-05'),
('tes1','tes1','2023-04-01');

/*Table structure for table `user` */

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `id_user` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `username` varchar(50) DEFAULT NULL,
  `pass` varchar(255) DEFAULT NULL,
  `role` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id_user`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;

/*Data for the table `user` */

insert  into `user`(`id_user`,`name`,`username`,`pass`,`role`) values 
(1,'admin','tes','tes','Admin'),
(2,'CJ','cj','202cb962ac59075b964b07152d234b70','Direksi'),
(3,'Charles','charles.csv','202cb962ac59075b964b07152d234b70','Programmer');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
